
public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice","Bob","Charlie","David"};
        double[] values = new double[4];

        System.out.println("Element of the numbers: ");
        for (int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }

        System.out.println("Element of names: ");
        for(int i = 0; i < names.length; i++){
            System.out.println(names[i]);
        }
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.println("Sum of number elements is: "+ sum);
    
    }
}



    
